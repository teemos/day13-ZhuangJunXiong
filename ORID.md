# ORID

## O

- React Router: The main function of React Router is to reduce the number of requests to backend servers, thereby reducing server pressure.
- Frontend and backend integration: In the afternoon, we learned about front-end and back-end integration, mainly using Axios to call simulated back-end data interfaces in the front-end.

## R

Good

## I

I haven't written enough front-end content before, and I may not have noticed the issue of page redirection. I have always used\<a\>tags for redirection. The downside of this is that every time I click on the tag, I have to request data from the backend. After using React Router, I don't need to initiate additional requests from the backend to achieve the same effect, and it also makes your thinking on writing front-end clearer.

## D

I am not very familiar with the front-end, and I will strengthen my learning of the front-end in the future.
