import axios from "axios";

const api = axios.create({
    // baseURL: "https://64c0b6530d8e251fd1126309.mockapi.io/api/v1/",
    baseURL: "http://localhost:8080",
});

export default api;