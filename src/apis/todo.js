import api from "./api"

export const getTodoTasks = () => {
    return api.get('/todo')
}

export const updateTodoTask = (id, todoTask) => {
    return api.put(`/todo/${id}`, todoTask)
}

export const deleteTodoTask = (id) => {
    return api.delete(`/todo/${id}`)
}

export const addTodoTask = (todoTask) => {
    return api.post(`/todo`, todoTask)
}
export const getTodoTaskById = (id) => {
    return api.get(`/todo/${id}`)
}