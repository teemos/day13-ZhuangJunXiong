import { useDispatch } from 'react-redux';
import * as todoApi from '../../apis/todo'
import { initTodoTasks } from "../todo/reducers/todoSlice";

export const useTodos = () => {
    const dispatch = useDispatch();
    async function loadTodos() {
        const response = await todoApi.getTodoTasks();
        dispatch(initTodoTasks(response.data));
    }
    const updateTodo = async (id, todoTask) => {
        await todoApi.updateTodoTask(id, todoTask);
        await loadTodos();
    }
    const deleteTodo = async (id) => {
        await todoApi.deleteTodoTask(id);
        await loadTodos();
    }
    const addTodo = async (taskName) => {
        await todoApi.addTodoTask({ name: taskName, done: false });
        await loadTodos();
    }
    const getTodoById = async (id) => {
        await todoApi.getTodoTaskById(id);
    }
    return {
        loadTodos,
        updateTodo,
        deleteTodo,
        addTodo,
        getTodoById,
    }
}