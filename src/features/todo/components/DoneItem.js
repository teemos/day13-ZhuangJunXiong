import { useNavigate } from 'react-router-dom';
import { useTodos } from "../../hooks/useTodos";

export default function DoneItem(props) {
    const navigate = useNavigate();
    const { deleteTodo } = useTodos();
    const handleTaskNameClick = () => {
        navigate('/done/' + props.task.id);
    }

    const handleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(props.task.id);
        }
    }
    return (
        <div className='todo-item'>
            <div className={`task-name ${props.task.done ? 'done' : ''}`}
                onClick={handleTaskNameClick}>{props.task.name}</div>
            <div className='remove-button' onClick={handleRemoveButtonClick}>x</div>
        </div>
    );
}