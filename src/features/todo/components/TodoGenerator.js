import { useState } from "react";
import { useTodos } from "../../hooks/useTodos";
import { Button } from 'antd';
import { Input } from 'antd';

export default function TodoGenerator() {
    const [taskName, setTaskName] = useState("");
    const { addTodo } = useTodos();

    const handleTaskNameChange = (e) => {
        const value = e.target.value;
        setTaskName(value);
    }

    const handleAddTodoTask = async () => {
        addTodo(taskName);
        setTaskName("");
    }
    return (
        <div className='todo-generator card'>
            <Input placeholder='input a new todo here...' onChange={handleTaskNameChange} value={taskName}></Input>
            <Button size={"large"} type="primary" onClick={handleAddTodoTask} disabled={!taskName} > Add </Button>
        </div>
    );
}