import { useTodos } from "../../hooks/useTodos";
import { Button, Card, Col, Modal, Row } from 'antd';
import { Input } from 'antd'
import React, { useState } from 'react';
import { DeleteOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";


export default function TodoItem(props) {
    const { updateTodo, deleteTodo, getTodoById } = useTodos();
    const [taskName, setTaskName] = useState(props.task.name);

    const [openDetailModal, setOpenDetailModal] = useState(false);
    const [openEditModal, setOpenEditModal] = useState(false);
    const [openDeleteModal, setOpenDeleteModal] = useState(false);

    const [confirmLoadingEdit, setConfirmLoadingEdit] = useState(false);
    const [confirmLoadingDelete, setConfirmLoadingDelete] = useState(false);


    const handleTaskNameChange = (e) => {
        const value = e.target.value;
        setTaskName(value);
    }
    const showDetailModal = async () => {
        setOpenDetailModal(true);
        await getTodoById(props.task.id)
    };
    const showEditModal = () => {
        setOpenEditModal(true);
    };
    const showDeleteModal = () => {
        setOpenDeleteModal(true);
    };

    const handleCancel = () => {
        console.log('Clicked cancel button');
        setOpenDetailModal(false);
        setOpenEditModal(false);
        setOpenDeleteModal(false);
    };

    const handleTaskNameClick = async () => {
        await updateTodo(props.task.id, { done: !props.task.done })
    }

    const handleRemoveButtonClick = async () => {
        setConfirmLoadingDelete(true);
        await deleteTodo(props.task.id);
        setConfirmLoadingDelete(false);
        setOpenDeleteModal(false);
    }
    const handleEditButtonClick = async () => {
        setConfirmLoadingEdit(true);
        await updateTodo(props.task.id, { name: taskName });
        setConfirmLoadingEdit(false);
        setOpenEditModal(false);
    }
    return (
        <Card className={'card'}>
            <Modal
                title="Edit todo text"
                open={openEditModal}
                onOk={handleEditButtonClick}
                confirmLoading={confirmLoadingEdit}
                onCancel={handleCancel}
            >
                <Input value={taskName} onChange={handleTaskNameChange} />
            </Modal>

            <Modal
                title="Delete todo"
                open={openDeleteModal}
                onOk={handleRemoveButtonClick}
                confirmLoading={confirmLoadingDelete}
                onCancel={handleCancel}
            >
                <p>Are you sure you wish to delete this item?</p>
            </Modal>

            <Modal
                title="Todo's detail"
                open={openDetailModal}
                onCancel={handleCancel}
                footer={<Button type="primary" onClick={handleCancel}>确定</Button>}
            >
                <p>{taskName}</p>
            </Modal>

            <Row className={'todo-row'}>
                <Col span={12} xs={12}>
                    <div className={`task-name ${props.task.done ? 'done' : ''}`}
                        onClick={handleTaskNameClick}>
                        {props.task.name}
                    </div>
                </Col>
                <Col span={4} xs={4}>
                    <Button onClick={showDetailModal}><EyeOutlined /></Button>
                </Col>
                <Col span={4} xs={4}>
                    <Button onClick={showEditModal}><EditOutlined /></Button>
                </Col>
                <Col span={4} xs={4}>
                    <Button onClick={showDeleteModal}><DeleteOutlined /></Button>
                </Col>
            </Row>

        </Card>
    );
}