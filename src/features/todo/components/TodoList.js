
import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";
import { useEffect } from "react";
import { useTodos } from "../../hooks/useTodos";

export default function TodoList() {
    const { loadTodos } = useTodos();
    useEffect(() => {
        loadTodos()
    }, [loadTodos])


    return (
        <div className='todo-list'>
            <h1>Todo List</h1>
            <TodoGroup />
            <TodoGenerator />
        </div>
    );
}