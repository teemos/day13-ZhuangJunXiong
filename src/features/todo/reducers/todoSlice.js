import { createSlice } from '@reduxjs/toolkit'

export const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        tasks: []
    },
    reducers: {
        initTodoTasks: (state, action) => {
            state.tasks = action.payload;
        },
    }
})

export const { initTodoTasks } = todoSlice.actions
export default todoSlice.reducer