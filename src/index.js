import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import store from './store'
import { Provider } from 'react-redux'

import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import ErrorPage from './pages/ErrorPage';
import HelpPage from './pages/HelpPage';
import DoneList from './features/todo/components/DoneList';
import TodoList from './features/todo/components/TodoList';
import DoneDetail from './features/todo/components/DoneDetail';

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        index: true,
        element: <TodoList />
      },
      {
        path: "/done",
        element: <DoneList />
      },
      {
        path: "/done/:id",
        element: <DoneDetail />
      },
      {
        path: "/help",
        element: <HelpPage />
      }
    ]
  },

])

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);
