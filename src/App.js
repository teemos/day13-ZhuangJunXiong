import './App.css';


import { Outlet, useNavigate } from 'react-router-dom'
import { useState } from "react";
import { Menu } from "antd";
import { AppstoreOutlined, HomeOutlined, QuestionCircleOutlined } from '@ant-design/icons';

function App() {
    const navigate = useNavigate();
    const [current, setCurrent] = useState('mail');
    const items = [
        {
            label: 'Home',
            key: '/',
            icon: <HomeOutlined />,
        },
        {
            label: 'Done',
            key: 'done',
            icon: <AppstoreOutlined />,
        },
        {
            label: 'Help',
            key: 'help',
            icon: <QuestionCircleOutlined />,
        },
    ];
    const onClick = (e) => {
        setCurrent(e.key);
        navigate(e.key);
    };
    return (
        <div >
            <div className={'todo'}>
                <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />
                <Outlet></Outlet>
            </div>

        </div>
    );
}
export default App;
